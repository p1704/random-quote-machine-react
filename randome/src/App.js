
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { fetchQuote } from './store/quote-actions';

import './App.css';

import QuoteMachine from './components/QuoteMachine';


function App() {
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(fetchQuote())
    }, [dispatch])

  return (
    <div className="App">
        <h1>Welcome to the Random Quote Machine!</h1>
        <QuoteMachine/>
    </div>
  );
}

export default App;
