import React from 'react'

import classes from './QuoteMachine.module.css'

import Quote from './Quote'
import QuoteBtn from './QuoteBtn'

const QuoteMachine = () => {
    return (
        <div className={classes.container}>
            <Quote/>
            <QuoteBtn/>
        </div>
    )
}

export default QuoteMachine
