import { createSlice } from '@reduxjs/toolkit'

const initialQuoteState = {
	quote: {
		message: 'I am the captain of my ship...',
		author: 'Nelson Mandela',
	},
}

const quoteSlice = createSlice({
	name: 'quote',
	initialState: initialQuoteState,
	reducers: {
		updateQuote(state, action) {
			state.quote.message = action.payload.quote
			state.quote.author = action.payload.author
		},
	},
})

export const quoteActions = quoteSlice.actions

export default quoteSlice.reducer
