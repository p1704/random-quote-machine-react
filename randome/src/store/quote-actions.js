import { quoteActions } from './quote-slice'

export const fetchQuote = () => {
	return async (dispatch) => {
		const fetchQuoteData = async () => {
			const response = await fetch('http://quotes.stormconsultancy.co.uk/random.json')

			if (!response.ok) {
				throw new Error('Something went wrong fetching data')
			}

			const responseData = await response.json()

			return responseData
		}

		try {
			const quote = await fetchQuoteData()
			dispatch(quoteActions.updateQuote(quote))
		} catch (error) {
			console.log(error)
		}
	}
}
