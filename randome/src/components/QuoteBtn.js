import React from 'react'
import { useDispatch } from 'react-redux'
import { fetchQuote } from '../store/quote-actions'
import classes from './QuoteBtn.module.css'

const QuoteBtn = () => {
	const dispatch = useDispatch()

	const getNewQouteHandler = (event) => {
		event.preventDefault()

		dispatch(fetchQuote())
	}

	return (
		<div className={classes.container}>
			<button onClick={getNewQouteHandler}>New qoute</button>
		</div>
	)
}

export default QuoteBtn
