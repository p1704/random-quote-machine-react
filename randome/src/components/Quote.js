import React from 'react'
import { useSelector } from 'react-redux'

import classes from './Quote.module.css'

const Quote = () => {
	const quote = useSelector((state) => state.quote.quote)

	let qouteContent = (
		<div className={classes.container}>
			<h5>{quote.message}</h5>
			<p>- {quote.author}</p>
		</div>
	)

	if (!quote.message) {
		qouteContent = (
			<div className={classes.container}>
				<h5>Click to generate the first quote</h5>
			</div>
		)
	}

	return <div>{qouteContent}</div>
}

export default Quote
